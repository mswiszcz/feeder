# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150621165327) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string   "title",              null: false
    t.text     "description"
    t.integer  "parent_category_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "categories", ["parent_category_id"], name: "index_categories_on_parent_category_id", using: :btree

  create_table "channels", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "title",            null: false
    t.text     "description"
    t.string   "site_url"
    t.string   "feed_url",         null: false
    t.datetime "checked_at"
    t.datetime "last_modified_at"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "channels", ["category_id"], name: "index_channels_on_category_id", using: :btree

  create_table "entries", force: :cascade do |t|
    t.integer  "channel_id"
    t.string   "title",                    null: false
    t.string   "site_url"
    t.string   "author"
    t.string   "guid",                     null: false
    t.text     "summary"
    t.text     "content"
    t.datetime "read_at"
    t.integer  "status",       default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.datetime "published_at"
  end

  add_index "entries", ["channel_id"], name: "index_entries_on_channel_id", using: :btree
  add_index "entries", ["guid"], name: "index_entries_on_guid", using: :btree

  add_foreign_key "channels", "categories"
  add_foreign_key "entries", "channels"
end
