class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title, null: false
      t.text :description
      t.references :parent_category, index: true

      t.timestamps null: false
    end
  end
end
