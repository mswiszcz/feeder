class CreateChannels < ActiveRecord::Migration
  def change
    create_table :channels do |t|
      t.belongs_to :category, index: true, foreign_key: true
      t.string :title, null: false
      t.text :description
      t.string :site_url
      t.string :feed_url, null: false
      t.datetime :checked_at
      t.datetime :last_modified_at

      t.timestamps null: false
    end
  end
end
