class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.belongs_to :channel, index: true, foreign_key: true
      t.string :title, null: false
      t.string :site_url
      t.string :author
      t.string :guid, index: true, null: false
      t.text :summary
      t.text :content
      t.datetime :read_at
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
