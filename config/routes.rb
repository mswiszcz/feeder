Rails.application.routes.draw do
  root to: 'entries#index'

  resources :categories
  resources :entries

  resources :channels do
    get :refresh
  end
end
