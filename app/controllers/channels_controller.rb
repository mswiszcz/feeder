class ChannelsController < ApplicationController
  expose(:channels)
  expose(:channel, attributes: :channel_params)

  def create
    channel = FetchChannel.call(params: channel_params).result

    if channel.save
      RefreshChannel.call(channel: channel)
      redirect_to channel, notice: t('.success')
    else
      render :new
    end
  end

  def update
    if channel.save
      redirect_to channel, notice: t('.success')
    else
      render :edit
    end
  end

  def refresh
    if RefreshChannel.call(channel: channel).result
      redirect_to channel, notice: t('.success')
    else
      redirect_to channel, error: t('.failure')
    end
  end

  def destroy
    channel.destroy
    redirect_to categories_path, notice: t('.success')
  end

  private

  def channel_params
    params.require(:channel).permit(:title, :description, :site_url, :feed_url, :category_id)
  end
end
