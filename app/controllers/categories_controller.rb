class CategoriesController < ApplicationController
  expose(:category, attributes: :category_params)

  def create
    if category.save
      redirect_to categories_path, notice: t('.success')
    else
      render :new
    end
  end

  def update
    if category.save
      redirect_to category, notice: t('.success')
    else
      render :edit
    end
  end

  def destroy
    category.destroy
    redirect_to :back, notice: t('.success')
  end

  private

  def category_params
    params.require(:category).permit(:title, :description)
  end
end
