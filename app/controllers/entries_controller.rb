class EntriesController < ApplicationController
  expose(:entries)
  expose(:entry)

  def destroy
    if entry.update(status: :read)
      render json: { status: 200 }
    else
      render json: { status: 420 }
    end
  end
end
