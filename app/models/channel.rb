class Channel < ActiveRecord::Base
  belongs_to :category, inverse_of: :channels
  has_many :entries, inverse_of: :channel, dependent: :destroy

  scope :without_category, -> { where(category_id: nil) }
end
