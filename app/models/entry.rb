class Entry < ActiveRecord::Base
  belongs_to :channel, inverse_of: :entries
end
