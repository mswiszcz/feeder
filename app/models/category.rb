class Category < ActiveRecord::Base
  has_many :channels, dependent: :nullify, inverse_of: :category
  has_many :entries, through: :channels
end
