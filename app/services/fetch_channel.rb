class FetchChannel < BusinessProcess::Base
  needs :params

  def call
    Channel.new(
      title: feed.title,
      site_url: feed.url,
      feed_url: feed.feed_url,
      category: category
    )
  end

  private

  def category
    @category ||= Category.find_by(id: params[:category_id])
  end

  def feed
    @feed ||= Feedjira::Feed.fetch_and_parse(params[:feed_url])
  end
end
