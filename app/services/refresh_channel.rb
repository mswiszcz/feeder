class RefreshChannel < BusinessProcess::Base
  needs :channel

  def call
    fetch_entries if feed_changed?
    channel.update(checked_at: Time.now, last_modified_at: feed.try(:last_modified))
  end

  private

  def feed
    @feed ||= Feedjira::Feed.fetch_and_parse(channel.feed_url)
  end

  def feed_changed?
    channel.last_modified_at.nil? || channel.last_modified_at < feed.last_modified
  end

  def fetch_entries
    feed.entries.first(10).each_with_index do |f, i|
      CreateEntry.call(entry_data: f, channel: channel) unless entry_exists?(f.id)
    end
  end

  def entry_exists?(guid)
    channel.entries.exists?(guid: guid)
  end
end
