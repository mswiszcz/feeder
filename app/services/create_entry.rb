class CreateEntry < BusinessProcess::Base
  include ActionView::Helpers::TextHelper
  
  needs :entry_data
  needs :channel

  def call
    entry = Entry.create(
      title: entry_data.title.sanitize,
      site_url: entry_data.url,
      author: entry_data.author,
      published_at: entry_data.published,
      guid: entry_data.id,
      summary: summary,
      content: content,
      channel: channel
    )
  end

  private

  def summary
    if entry_data.summary
      entry_data.summary.sanitize.gsub(/<\/?[^>]*>/, '')
    elsif entry_data.content
      truncate((entry_data.content.sanitize).gsub(/<\/?[^>]*>/, ''), length: 300, separator: ' ')
    end
  end

  def content
    if entry_data.content
      entry_data.content.sanitize
    elsif entry_data.summary
      entry_data.summary + ' [...]'
    end
  end
end
