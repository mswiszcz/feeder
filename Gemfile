source 'https://rubygems.org'
ruby '2.2.1'

gem 'rails', '4.2.1'
gem 'pg', '~> 0.18.1'

# Assets
gem 'sass-rails', '~> 5.0.1'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails', '~> 4.0.3'
gem 'turbolinks', '~> 2.5.3'

gem 'bourbon', '~> 4.2.2'
gem 'yui-compressor', '~> 0.12.0'
gem 'uglifier', '~> 2.7.1'

# Views
gem 'slim-rails', '~> 3.0.1'
gem 'draper', '~> 2.1.0'
gem 'simple_form', '~> 3.1.0'

# Utilities
gem 'feedjira', '~> 2.0.0'
gem 'business_process', '~> 0.0.1'
gem 'metainspector', '~> 4.6.0'
gem 'decent_exposure', '~> 2.3.2'
gem 'decent_decoration', '~> 0.0.6'

group :development do
  gem 'better_errors', '~> 2.1.0'
  gem 'binding_of_caller', '~> 0.7.2'
  gem 'bullet', '~> 4.14.0'

  gem 'capistrano-bundler', '~> 1.1.3'
  gem 'capistrano-rails-console', '~> 0.5.2'
  gem 'capistrano-rails', '~> 1.1.2'
  gem 'capistrano-rvm', '~> 0.1.2'
  gem 'capistrano', '~> 3.4.0'

  gem 'guard-bundler', '~> 2.1.0'
  gem 'guard-rails', '~> 0.7.0'
  gem 'guard-rspec', '~> 4.5.0'
  gem 'quiet_assets', '~> 1.1.0'

  gem 'thin', '~> 1.6.3'
end

group :development, :test do
  gem 'factory_girl_rails', '~> 4.5.0'
  gem 'ffaker', '~> 2.0.0'

  gem 'pry-byebug', '~> 3.1.0'
  gem 'pry-rails', '~> 0.3.2'
  gem 'pry-rescue', '~> 1.4.1'

  gem 'rspec-its', '~> 1.2.0'
  gem 'rspec-rails', '~> 3.3.1'
  gem 'shoulda', '~> 3.5.0'

  gem 'spring-commands-rspec', '~> 1.0.4'
  gem 'spring', '~> 1.3.3'
end
